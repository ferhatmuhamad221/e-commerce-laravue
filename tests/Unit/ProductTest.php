<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
// use Tests\TestCase;
use App\Http\Controllers\ProductController;
use App\Models\Product;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertJson;

class ProductTest extends TestCase
{

    use WithFaker;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $this->assertTrue(true);
    // }

    public function test_can_get_product()
    {
        $value = [
            'name' => $this->faker->name,
            'slug' => $this->faker->slug,
            'type' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'price' => $this->faker->sentence,
            'quantity' => $this->faker->sentence,
        ]; //factory(Product::class)->create();
        $this->get(route('product.all'))
            ->assertStatus(200)
            ->assertJson($value);
    }
}
