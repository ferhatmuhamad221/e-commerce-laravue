<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'uuid' => 'TRX' . $faker->numerify,
        'name' => $faker->name,
        'email' => $faker->email,
        'number' => $faker->phoneNumber,
        'address' => $faker->sentence,
        'transaction_total' => 100,
        'transaction_status' => 'PENDING',
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
