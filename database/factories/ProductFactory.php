<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'type' => $faker->sentence,
        'description' => $faker->paragraph,
        'price' => $faker->sentence,
        'quantity' => $faker->sentence,
    ];
});
