<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\CheckoutRequest;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function checkout(CheckoutRequest $request)
    {
        // Masukkan request kecuali (except)
        $data = $request->except('transaction_details');
        // mt_rand generate integer random
        $data['uuid'] = 'TRX' . mt_rand(10000, 99999) . mt_rand(100, 999);

        $transaction = Transaction::create($data);

        foreach ($request->transaction_details as $product) {

            // Setelah transaksi dibuat di barus 21 => selanutnya panggil details dengan transaksi id
            // setelah terpanggil kita panggil product dari array tersebut.
            $details[] = new TransactionDetail([
                'transactions_id' => $transaction->id,
                'products_id' => $product,
            ]);

            Product::find($product)->decrement('quantity');
        }

        // Transactions relasi dengan details lalu saveMany 
        $transaction->details()->saveMany($details);

        return ResponseFormatter::success($transaction);
        return response()->json($transaction, 200);
    }
}
