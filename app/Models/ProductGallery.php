<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGallery extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'products_id', 'photo', 'is_default'
    ];

    protected $hidden = [];

    public function product()
    {
        // belongsTo => Product Gallery miliknya si Product
        // products_id, id => id yang berelasi
        return $this->belongsTo(Product::class, 'products_id', 'id');
    }

    // Menggunakan Accessor => Mengubah data gambar saat diambil dari database (API) nantinya
    public function getPhotoAttribute($value)
    {
        return url('storage/' . $value);
    }
}
