<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    // use HasFactory;

    protected $fillable = [
        'name', 'type', 'description', 'price', 'slug', 'quantity'
    ];

    protected $hidden = [];

    public function galleries()
    {
        // hasMany => bahwa product memiliki bagian lainnya/relasi yaitu galeri
        return $this->hasMany(ProductGallery::class, 'products_id');
    }

    // public function transaction()
    // {
    //     return $this->hasMany(TransactionDetail::class, 'products_id');
    // }
}
