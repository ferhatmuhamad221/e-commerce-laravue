<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'uuid', 'name', 'email', 'number', 'address', 'transaction_total', 'transaction_status'
    ];

    protected $hidden = [];

    public function details()
    {
        // hasMany => bahwa product memiliki bagian lainnya/relasi yaitu galeri, 
        // di ikuti dengan 'ID database lain yg berelasi'
        return $this->hasMany(TransactionDetail::class, 'transactions_id');
    }
}
