<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    
    {{-- META --}}
    @include('includes.meta')

    {{-- STACK --}}
    @stack('before-style')

    {{-- STYLE --}}
    @include('includes.style')

    {{-- STACK => Fungsi untuk memberikan script/style yang hanya dapat diakses satu halaman atau beberapa aja --}}
    {{-- Nantinya STACK akan dipanggil melalui @push() --}}
    @stack('after-style')
    
</head>

<body>

    {{-- LEFT PANEL / SIDEBAR --}}
    @include('includes.sidebar')
    
    <div id="right-panel" class="right-panel">
        
        {{-- HEADER / NAVBAR --}}
        @include('includes.navbar')

        <div class="content">

            {{-- CONTENT (PAGES/DASHBOARD) --}}
            {{-- Menggunakan Yield maksudnya bagian yg akan kita isi (Content) --}}
            @yield('content')

        </div>
        <div class="clearfix"></div>
    </div>

    {{-- SCRIPT --}}
    @stack('before-script')
    @include('includes.script')
    @stack('after-script')
    
</body>

</html>